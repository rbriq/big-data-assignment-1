package de.bigdata.solution.assignment2.bolts;

import java.util.HashMap;
import java.util.Map;

import de.bigdata.solution.assignment2.CountDistinctContainer;
import de.bigdata.solution.assignment2.clustermodel.CountDistinctClusterModel;
import de.fraunhofer.iais.kd.livlab.bda.clustermodel.ClusterModel;
import de.fraunhofer.iais.kd.livlab.bda.clustermodel.ClusterModelFactory;
import de.fraunhofer.iais.kd.livlab.bda.config.BdaConstants;

/**
 * @author mmock Count Distinct
 */

public class ArtistCountDistinctDetecor {

	private final int count = 0;
	private final ClusterModel model;
	private final CountDistinctClusterModel countClusterModel;
	private final Map <String,CountDistinctContainer>  countDistinctContainerMap = new HashMap<String, CountDistinctContainer>();

	public ArtistCountDistinctDetecor() {
		model = ClusterModelFactory
				.readFromCsvResource(BdaConstants.CLUSTER_MODEL);
		countClusterModel = new CountDistinctClusterModel(model);

	}

	public String[] process(String userid, String artid, String artname) {
		if(!countDistinctContainerMap.containsKey(artid)) {
			countDistinctContainerMap.put(artid, new CountDistinctContainer(artname));
		}
		countDistinctContainerMap.get(artid).addUser(userid);
		CountDistinctContainer currentContainer = countDistinctContainerMap.get(artid);
		if(currentContainer.isIncreased()) {
			String[] result = new String[3];
			result[0] = artname;
			result[1] = String.valueOf(currentContainer.getCount());
			result[2] = countClusterModel.getClosest(currentContainer.getSketch());


		}
		return null;
	}

}
