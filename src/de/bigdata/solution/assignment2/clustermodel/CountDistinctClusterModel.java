package de.bigdata.solution.assignment2.clustermodel;

import de.bigdata.solution.assignment2.dataStructures.CountDistinctSketch;
import de.fraunhofer.iais.kd.livlab.bda.clustermodel.ClusterModel;

/**
 * task 2
 * @author Rania
 *
 */
public class CountDistinctClusterModel {

	private final ClusterModel clusterModel;

	public CountDistinctClusterModel(ClusterModel clusterModel) {

		this.clusterModel = clusterModel;
	}

	public String getClosest(CountDistinctSketch sketch) {
		Double minDistance = Double.MAX_VALUE;
		String minClusterId = "-1";

		for(String clusterId : clusterModel. getKeys()) {
			String curClusterData = clusterModel.get(clusterId);
			CountDistinctSketch other = constructUserSketch(curClusterData);
			double curDistance = sketch.getDistanceTo(other);
			if(curDistance < minDistance) {
				minDistance = curDistance;
				minClusterId = clusterId;
			}
		}
		return minClusterId;

	}


	private CountDistinctSketch constructUserSketch(String userSetStr) {
		CountDistinctSketch userSketch = new CountDistinctSketch(CountDistinctSketch.MAX_USERS_COUNT);
		String [] userSetArr = userSetStr.split(",");
		for(int i = 0; i < userSetArr.length; i++) {
			int k = i + 1;
			userSketch.addUser("user_" + k);
		}
		return userSketch;
	}


}
