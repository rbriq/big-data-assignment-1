package de.bigdata.solution.assignment2;

import de.bigdata.solution.assignment2.dataStructures.CountDistinctSketch;

/**
 * Task 2
 * @author
 *
 */
public class CountDistinctContainer {

	private final CountDistinctSketch userSketch = new CountDistinctSketch(0);
	private double prevEstimate = 0;
	private double currentEstimate = 0;
	private final String artName;

	public CountDistinctContainer(String artName) {
		this.artName = artName;
		userSketch.setArtName(artName);
	}
	// add a user to CountDistinctSketch
	public void addUser(String username) {
		prevEstimate = currentEstimate;
		userSketch.addUser(username);
		currentEstimate = userSketch.getEstimate();
	}


	/**
	 * get estimation of how many distinct users are added in the sketch
	 * @return
	 */
	public int getCount() {
		return (int)userSketch.getEstimate();
	}

	/**
	 * Returns true iff last addUser was increasing the estimate
	 * @return
	 */
	public boolean isIncreased() {
		return currentEstimate > prevEstimate ? true : false;
	}
	/**
	 * gets the contained sketch
	 * @return
	 */
	public CountDistinctSketch getSketch() {
		return userSketch;
	}

	/**
	 * gets the artname
	 * @return
	 */
	public String getArtname() {
		return artName;
	}

}
