package de.bigdata.solution.assignment2.dataStructures;

import java.util.BitSet;

/**
 * Task 1 Stores a linear sketch representation of a userset
 *
 * @author
 *
 */
public class CountDistinctSketch {

	public static final int MAX_USERS_COUNT = 1000;

	/**
	 * the artist id for whom we are creating the sketch (sketch of usernames
	 * with listening event for an artist)
	 */
	private String artName;

	private final BitSet userSketch;
	private final int sketchSize;

	public CountDistinctSketch(int sketchSize) {
		this.sketchSize = sketchSize;
		userSketch = new BitSet();
	}

	// add a user to an countDistinctSketch intstance
	public void addUser(String username) {
		int hashCode = Math.abs(username.hashCode()) % MAX_USERS_COUNT;
		userSketch.set(hashCode);
	}



	/**
	 * get estimation of how many distinct users are added in the sketch
	 *
	 * @return
	 */

	public double getEstimate() {
		return getEstimate(userSketch);
	}

	private double getEstimate(BitSet bitSet) {
		int m = bitSet.size();
		int uS =  bitSet.size() - bitSet.cardinality();
		if(m==0 || uS==0) {
			return 0;
		}
		double estimate = -m * Math.log(uS/m);
		return estimate;
	}
	/**
	 * compute Jaccard distance to another countDistinctSketch
	 *
	 * @return
	 */
	public double getDistanceTo(CountDistinctSketch otherUserSketch) {
		double distance = (getEstimate() + otherUserSketch.getEstimate()
				- getUnionEstimate(otherUserSketch.getUserSketch()))
				/ getUnionEstimate(otherUserSketch.getUserSketch());
		return 1 - distance;
	}

	/**
	 * return the estimate of the union of 2 sets
	 * @param otherUserSketch
	 * @return
	 */
	private double getUnionEstimate(BitSet otherUserSketch) {
		BitSet newSketch = new BitSet();
		newSketch.or(userSketch);
		newSketch.or(otherUserSketch);
		return getEstimate(newSketch);
	}
	/**
	 * return the estimate of intersection between 2 user sketches
	 * @param otherUserSketch
	 * @return
	 */
	private double getIntersectionEstimate(CountDistinctSketch otherUserSketch) {
		double estimate = getEstimate() + otherUserSketch.getEstimate() - getUnionEstimate(otherUserSketch.getUserSketch());

		return estimate;
	}

	/*private int getUnionSize(BitSet otherUserSketch) {
		BitSet newSketch = new BitSet();
		newSketch.or(userSketch);
		newSketch.or(otherUserSketch);
		return newSketch.cardinality();
	}*/

	/**
	 *
	 * @return the userset
	 */
	public BitSet getUserSketch() {
		return userSketch;
	}

	public void setArtName(String artName) {
		this.artName = artName;

	}
}
